<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */


if (TL_MODE === 'BE') {
    $GLOBALS['TL_CSS'][] = 'bundles/contaonewstweets/css/newstweet_backend.css';
}

$GLOBALS['BE_MOD']['content']['news']['tables'][] = 'tl_news_twitter_accounts';

$GLOBALS['TL_MODELS']['tl_news_twitter_accounts'] = JedoLabs\ContaoNewstweetsBundle\Model\NewsTwitterAccountsModel::class;

// Languages Settings for fieldsets, legends and more
$GLOBALS['TL_LANG']['tl_news_archive']['newstweeting_legend']           = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_archive.newstweeting_legend');
$GLOBALS['TL_LANG']['tl_news_twitter_accounts']['new']['0']             = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_twitter_accounts.new.0');
$GLOBALS['TL_LANG']['tl_news_twitter_accounts']['new']['1']             = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_twitter_accounts.new.1');
$GLOBALS['TL_LANG']['tl_news_twitter_accounts']['account_legend']       = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_twitter_accounts.account_legend');
$GLOBALS['TL_LANG']['tl_news_twitter_accounts']['settings_legend']      = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_twitter_accounts.settings_legend');
$GLOBALS['TL_LANG']['tl_news_twitter_accounts']['tweetsettings_legend'] = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_twitter_accounts.tweetsettings_legend');
$GLOBALS['TL_LANG']['tl_news_twitter_accounts']['publish_legend']       = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news_twitter_accounts.publish_legend');
$GLOBALS['TL_LANG']['tl_news']['TweetOnTwitter_legend']                 = \System::getContainer()->get('translator')->trans('jedolabs.newstweets.tl_news.TweetOnTwitter_legend');
