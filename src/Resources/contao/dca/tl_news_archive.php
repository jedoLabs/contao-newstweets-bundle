<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

use Contao\NewsArchiveModel;

$translator = \System::getContainer()->get('translator');

$dca = &$GLOBALS['TL_DCA'][NewsArchiveModel::getTable()];

if (\Database::getInstance()->fieldExists('SendNewsToTwitter', NewsArchiveModel::getTable())) {

        $dca['palettes']['__selector__'][] = 'SendNewsToTwitter';
        $dca['subpalettes']['SendNewsToTwitter'] = 'UsedTwitterAccount';

        \Contao\CoreBundle\DataContainer\PaletteManipulator::create()
            ->addLegend('newstweeting_legend', 'comment_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_AFTER)
            ->addField('SendNewsToTwitter', 'newstweeting_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
            ->applyToPalette('default', 'tl_news_archive');

        /*
         * Add global operations
         */
        array_insert(
            $dca['list']['global_operations'], 1, [
                'TwitterAccounts' => [
                    'label' => [
                        $translator->trans('jedolabs.newstweets.tl_news_archive.TwitterAccounts.0'),
                        $translator->trans('jedolabs.newstweets.tl_news_archive.TwitterAccounts.1'),
                    ],
                    'href' => 'table=tl_news_twitter_accounts',
                    'class' => 'header_twitter',
                    'icon' => 'bundles/contaonewstweets/images/WhiteOnBlue.svg',
                    'attributes' => 'onclick="Backend.getScrollOffset()"',
                ],
            ]
        );
        array_insert(
            $dca['list']['operations'], 4, [
                'toggleTweetArchive' => [
                    'label' => [
                        $translator->trans('jedolabs.newstweets.tl_news_archive.toggleTweetArchive.0'),
                        $translator->trans('jedolabs.newstweets.tl_news_archive.toggleTweetArchive.1'),
                    ],
                    'showInHeader' => true,
                    'icon' => 'bundles/contaonewstweets/images/WhiteOnBlue.svg',
                    'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleTwitterArchiveVisibility(this,%s)"',
                    'button_callback' => ['jedoslabs.newstweets.listener.data_container.news_archive_listener', 'toggleTwitterArchive'],
                ],
            ]
        );
}

$fields = [
    'SendNewsToTwitter' => [
        'label' => [
            $translator->trans('jedolabs.newstweets.tl_news_archive.SendNewsToTwitter.0'),
            $translator->trans('jedolabs.newstweets.tl_news_archive.SendNewsToTwitter.1'),
        ],
        'inputType' => 'checkbox',
        'eval' => ['mandatory' => false, 'submitOnChange' => true, 'tl_class' => 'w50 m12 clr'],
        'sql' => "char(1) NOT NULL default ''",
    ],
    'UsedTwitterAccount' => [
        'label' => [
            $translator->trans('jedolabs.newstweets.tl_news_archive.UsedTwitterAccount.0'),
            $translator->trans('jedolabs.newstweets.tl_news_archive.UsedTwitterAccount.1'),
        ],
        'exclude' => true,
        'filter' => true,
        'inputType' => 'select',
        //'foreignKey' => 'tl_news_twitter_accounts.account',
        'options_callback' 		=> ['jedoslabs.newstweets.listener.data_container.news_archive_listener', 'getTwitterAccoutOptions'],
        'eval' => ['mandatory' => true, 'includeBlankOption' => true, 'choosen' => true, 'tl_class' => 'w50'],
        'sql' => "int(10) unsigned NOT NULL default '0'",
        //'relation' => ['type' => 'hasOne', 'load' => 'eager'],
    ],
];

$dca['fields'] = array_merge($dca['fields'], $fields);

