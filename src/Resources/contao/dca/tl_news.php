<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

use Contao\NewsArchiveModel;
use Contao\NewsModel;

$translator = \System::getContainer()->get('translator');

$dca = &$GLOBALS['TL_DCA'][NewsModel::getTable()];

if (\Database::getInstance()->fieldExists('SendNewsToTwitter', NewsArchiveModel::getTable())) {


    $objArchive = \Database::getInstance()->prepare('SELECT pid FROM ' . NewsModel::getTable() . ' WHERE id=?')
        ->limit(1)
        ->execute(\Input::get('id'));

    $achiveID = $objArchive->pid;

    $result = \Database::getInstance()->prepare('SELECT SendNewsToTwitter FROM ' . NewsArchiveModel::getTable() . ' WHERE id=?')->execute($achiveID);
    $SendNewsToTwitter = $result->SendNewsToTwitter;


    if ("" !== $SendNewsToTwitter) {
        $dca['config']['onsubmit_callback'][] = ['jedoslabs.newstweets.listener.data_container.news_listener', 'SendThisNewsAfterSubmit'];

        array_insert(
            $dca['list']['operations'], 6, [
                'toggleTweeting' => [
                    'label' => [
                        $translator->trans('jedolabs.newstweets.tl_news.toggleTweeting.0'),
                        $translator->trans('jedolabs.newstweets.tl_news.toggleTweeting.1'),
                    ],
                    'icon' => 'bundles/contaonewstweets/images/WhiteOnBlue.svg',
                    'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleTwitterNewsVisibility(this,%s)"',
                    'button_callback' => ['jedoslabs.newstweets.listener.data_container.news_listener', 'toggleTwitterNews'],
                ],
            ]
        );

        $dca['palettes']['__selector__'][] = 'TweetOnTwitter';
        $dca['palettes']['__selector__'][] = 'TweetCustomizing';

        $dca['subpalettes']['TweetOnTwitter'] = 'TweetWithImages, TweetCustomizing';
        $dca['subpalettes']['TweetCustomizing'] = 'CustomTweetText,CustomTweetImages';

        \Contao\CoreBundle\DataContainer\PaletteManipulator::create()
            ->addLegend('TweetOnTwitter_legend', 'publish_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_AFTER)
            ->addField('TweetOnTwitter', 'TweetOnTwitter_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
            ->applyToPalette('default', 'tl_news');
    }
}

$fields = [
    'NewsTweetID' => [
        'sql' => ['type' => 'string', 'default' => ''],
    ],
    'TweetOnTwitter' => [
        'label' => [
            $translator->trans('jedolabs.newstweets.tl_news.TweetOnTwitter.0'),
            $translator->trans('jedolabs.newstweets.tl_news.TweetOnTwitter.1'),
        ],
        'inputType' => 'checkbox',
        'eval' => ['submitOnChange' => true, 'tl_class' => 'w50 m12'],
        'sql' => "char(1) NOT NULL default ''",
    ],
    'TweetWithImages' => [
        'label' => [
            $translator->trans('jedolabs.newstweets.tl_news.TweetWithImages.0'),
            $translator->trans('jedolabs.newstweets.tl_news.TweetWithImages.1'),
        ],
        'inputType' => 'checkbox',
        'eval' => ['tl_class' => 'w50 m12'],
        'sql' => ['type' => 'boolean', 'default' => 0],
    ],
    'TweetCustomizing' => [
        'label' => [
            $translator->trans('jedolabs.newstweets.tl_news.TweetCustomizing.0'),
            $translator->trans('jedolabs.newstweets.tl_news.TweetCustomizing.1'),
        ],
        'inputType' => 'checkbox',
        'eval' => ['submitOnChange' => true, 'tl_class' => 'w50 m12'],
        'sql' => ['type' => 'boolean', 'default' => 0],
    ],
    'CustomTweetText' => [
        'label' => [
            $translator->trans('jedolabs.newstweets.tl_news.CustomTweetText.0'),
            $translator->trans('jedolabs.newstweets.tl_news.CustomTweetText.1'),
        ],
        'exclude' => true,
        'search' => true,
        'inputType' => 'textarea',
        'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'tl_class' => 'clr'],
        'sql' => ['type' => 'text', 'notnull' => false],
    ],
    'CustomTweetImages' => [
        'label' => [
            $translator->trans('jedolabs.newstweets.tl_news.CustomTweetImages.0'),
            $translator->trans('jedolabs.newstweets.tl_news.CustomTweetImages.1'),
        ],
        'exclude' => true,
        'inputType' => 'fileTree',
        'eval' => [
            'files' => true,
            'filesOnly' => true,
            'fieldType' => 'radio',
            'extensions' => \Contao\Config::get('validImageTypes'),
            'tl_class' => 'clr',
        ],
        'sql' => ['type' => 'binary', 'length' => 16, 'notnull' => false],
    ],
];

$dca['fields'] = array_merge($dca['fields'], $fields);
