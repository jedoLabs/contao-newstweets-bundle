<?php

/*
 * This file is part of NewsTweetsBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     Contao4 NewsTweetsBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

declare(strict_types=1);

namespace JedoLabs\ContaoNewstweetsBundle\Library;

use Contao\NewsArchiveModel;
use Contao\NewsModel;
use JedoLabs\ContaoNewstweetsBundle\Model\NewsTwitterAccountsModel;

class NewsTweets
{
    public function removeTweetByNewsID($intId)
    {
        $article = \Database::getInstance()->prepare('SELECT n.*, na.id, na.jumpTo, na.SendNewsToTwitter, na.UsedTwitterAccount FROM '.NewsModel::getTable().' as n
                JOIN '.NewsArchiveModel::getTable().' as na ON(n.pid = na.id) 
                WHERE n.id = ?')
            ->limit(1)
            ->execute($intId);

        /* fetch associative array */
        while ($nrow = $article->fetchAssoc()) {
            $TwitterAccountSettings = self::getTwitterAccountData($nrow['UsedTwitterAccount']);

            $twitterAccess = new TwitterAccess(
                $TwitterAccountSettings['consumerKey'],
                $TwitterAccountSettings['consumerSecret'],
                $TwitterAccountSettings['accessKey'],
                $TwitterAccountSettings['accessSecret']
            );

            if ($nrow['NewsTweetID']) {
                $twitterAccess->removeTweet($nrow['NewsTweetID']);
            }

            // Update the database
            \Database::getInstance()->prepare('
            UPDATE '.NewsModel::getTable().' SET tstamp='.time().', NewsTweetID="" WHERE id = ? ')->execute($intId);
        }
    }

    public function tweetByNewsID($intId)
    {
        $tweetText = 0;
        $tweetImage = 0;

        $article = \Database::getInstance()->prepare('SELECT n.*, na.id, na.jumpTo, na.SendNewsToTwitter, na.UsedTwitterAccount FROM '.NewsModel::getTable().' as n JOIN '.NewsArchiveModel::getTable().' as na ON(n.pid = na.id) WHERE n.id = ?')
            ->limit(1)
            ->execute($intId);

        /* fetch associative array */
        while ($nrow = $article->fetchAssoc()) {
            $TwitterAccountSettings = self::getTwitterAccountData($nrow['UsedTwitterAccount']);

            $twitterAccess = new TwitterAccess(
                $TwitterAccountSettings['consumerKey'],
                $TwitterAccountSettings['consumerSecret'],
                $TwitterAccountSettings['accessKey'],
                $TwitterAccountSettings['accessSecret']
            );

            if ($nrow['NewsTweetID']) {
                $twitterAccess->removeTweet($nrow['NewsTweetID']);
            }

            if ($nrow['TweetWithImages']) {
                // Check for images
                if ($nrow['CustomTweetImages'] || $nrow['singleSRC']) {
                    if ($nrow['CustomTweetImages'] && $nrow['TweetCustomizing']) {
                        $imageID = $nrow['CustomTweetImages'];
                    } elseif ('' !== $nrow['singleSRC']) {
                        $imageID = $nrow['singleSRC'];
                    }

                    $objFile = \FilesModel::findByUuid($imageID);

                    if (null === $objFile || !is_file(TL_ROOT.'/'.$objFile->path)) {
                        return '';
                    }
                    $tweetImage = 1;
                    $picture = $objFile->path;
                }
            }
            // Check for post text
            if ($nrow['TweetCustomizing'] && $nrow['CustomTweetText']) {
                $message = $nrow['CustomTweetText'];
                $tweetText = 1;
            } else {
                $message = $nrow['teaser'];
                $tweetText = 1;
            }

            if ($tweetText = 0) {
                return;
            }

            // Ersetze Sonderzeichen
            $searchArray = ['&#35;', '&#40;', '&#41;', '&#60;', '&#61;', '&#62;', '&#92;'];
            $replaceArray = ['#', '(', ')', '<', '=', '>', '\\'];
            $plaintext_message = str_replace($searchArray, $replaceArray, $message);

            //URL adden?
            if ($TwitterAccountSettings['addUrl']) {
                $url = ShortUrl::getUrl($nrow, $TwitterAccountSettings['urlPrefix']);

                if ($TwitterAccountSettings['useShortUrl']) {
                    $shortUrl = ShortUrl::getTinyUrl($url);
                } elseif ($TwitterAccountSettings['useShortUrl']) {
                    $shortUrl = ShortUrl::getBitlyUrl($url, $TwitterAccountSettings['bitlyLogin'], $TwitterAccountSettings['bitlyAPIKey']);
                } else {
                    $shortUrl = $url;
                }

                $urlLength = \strlen($shortUrl);
                $shortText = self::shortText($plaintext_message, 279 - $urlLength);

                $plaintext_message = $shortText.' '.$shortUrl;
            } else {
                $plaintext_message = self::shortText($message, 280);
            }

            $twitterAccess->sendPost($plaintext_message, $picture);

            $TweetID = $twitterAccess->findLastTweetforID($TwitterAccountSettings['account']);

            // Update the database
            \Database::getInstance()->prepare('
            UPDATE '.NewsModel::getTable().' SET tstamp='.time().', NewsTweetID='.$TweetID.' WHERE id = ? ')->execute($intId);
        }
    }

    private function getTwitterAccountData($AccountID)
    {
        $return = [];

        $usedAccountData = \Database::getInstance()->prepare('Select * from '.NewsTwitterAccountsModel::getTable().' where id = ?')
            ->limit(1)
            ->execute($AccountID);

        // Twitter Acccount ID
        $return['user_id'] = $usedAccountData->user_id;
        $return['account'] = $usedAccountData->account;

        //Twitter Api Settings
        //$return['consumerKey'] = Encryption::decryptData($usedAccountData->consumerKey);
        $return['consumerKey'] = \System::getContainer()->get('jedolabs.extensionhelper.security.encryption')->decryptData($usedAccountData->consumerKey);
        //$return['consumerSecret'] = Encryption::decryptData($usedAccountData->consumerSecret);
        $return['consumerSecret'] = \System::getContainer()->get('jedolabs.extensionhelper.security.encryption')->decryptData($usedAccountData->consumerSecret);
        //$return['accessKey'] = Encryption::decryptData($usedAccountData->accessKey);
        $return['accessKey'] = \System::getContainer()->get('jedolabs.extensionhelper.security.encryption')->decryptData($usedAccountData->accessKey);
        //$return['accessSecret'] = Encryption::decryptData($usedAccountData->accessSecret);
        $return['accessSecret'] = \System::getContainer()->get('jedolabs.extensionhelper.security.encryption')->decryptData($usedAccountData->accessSecret);

        //Tweet Settings
        $return['addUrl'] = $usedAccountData->addUrl;
        $return['urlPrefix'] = $usedAccountData->urlPrefix;
        $return['useShortUrl'] = $usedAccountData->useShortUrl;
        $return['activateBitly'] = $usedAccountData->activateBitly;
        //$return['bitlyLogin'] = Encryption::decryptData($usedAccountData->bitlyLogin);
        $return['bitlyLogin'] = \System::getContainer()->get('jedolabs.extensionhelper.security.encryption')->decryptData($usedAccountData->bitlyLogin);
        //$return['bitlyAPIKey'] = Encryption::decryptData($usedAccountData->bitlyAPIKey);
        $return['bitlyAPIKey'] = \System::getContainer()->get('jedolabs.extensionhelper.security.encryption')->decryptData($usedAccountData->bitlyAPIKey);


        return $return;
    }

    /*
     *  Tweet text short at 280 characters
     */
    private function shortText($text, $textLength = 280, $ending = ' ...')
    {
        //HTML und Insert-Tags entfernen
        $text = strip_tags($text);
        $text = preg_replace('/\{\{.*\}\}/', '', $text);

        if (\strlen($text) <= $length) {
            return $text;
        }
        $length = 0;
        $maxLength = $textLength - \strlen($ending);
        $shortText = '';
        $delimiter = '';
        $words = explode(' ', $text);

        foreach ($words as $pos => $word) {
            $wordLength = \strlen($word);
            if ($pos > 0) {
                $delimiter = ' ';
            }

            if ($length + $wordLength + \strlen($delimiter) <= $maxLength) {
                $shortText .= $delimiter.$word;
                $length += $wordLength + \strlen($delimiter);
            } else {
                break;
            }
        }

        return $shortText.$ending;
    }
}
